# -*- encoding: utf-8 -*-
'''
文件:    Experiment1.py
时间:    2020/04/11 13:46:27
作者:    20192426 陈瀚文
'''

# 这是单行注释
'''
这是多行注释
其本质是一个字符串
'''
a = 13
b = 1.3
c = "Python太棒了！"
# 输出a,b,c的数据类型
print("a:", type(a), "\nb:", type(b), "\nc:", type(c))
# 输出a,b,c对应的值，format方法可以将字符串对象格式化输出。
print("a=", a, "\nb=", b, "\n{}".format(c))
# 使用循环打印九九乘法表
for i in range(1, 10):
    for j in range(1, i + 1):
        print(str(j)+'×'+str(i) + '=' + str(i * j), end="\t")
    print()

# -*- encoding: utf-8 -*-
'''
文件:    Experiment2.py
时间:    2020/04/11 16:12:24
作者:    20192426 陈瀚文
'''

# 版本一


def judgesymbol(a):
    '''
    判断是否输入的符号是否为+-*/%中的一个
    '''
    if a in ["+", "-", "*", "/", "%", "**"]:
        return 1
    else:
        print("\033[31m请重新输入运算符号！\033[0m")
        return 0


def judgezero(a):
    '''
    功能：判断被除数是否为零，若不为零输出运算结果，否则重新输入
    '''
    try:
        eval(a)
        return 1
    except ZeroDivisionError:
        print("\033[31m0不能作为除数！请重新输入！\033[0m")
        return 0


i = 2
sum = ""
sum1 = ""
while True:
    if i == 0:
        break
    if i == 1:              # 对得出的结果继续操作
        sum1 = sum
        print("第一个操作数为", eval(sum1))
        a = input("请输入运算类型:")
        if judgesymbol(a) == 0:     # 判断符号是否合法
            continue
        b = input("请输入第二个操作数：")
        sum1 = sum+a+b
        if judgezero(sum1) == 0:    # 判断是否进行了除0运算
            continue
        print("{}".format(eval(sum))+a+b+"={:g}".format(eval(sum1)))
        sum = sum1
        i = int(input("是否继续输入？(输入0结束，输入1对得出的结果继续操作，输入2进行新的运算)"))
    if i == 2:          # 对两个数进行运算
        a = input("请输入第一个操作数：")
        s = input("请输入运算类型：")
        if judgesymbol(s) == 0:     # 判断符号是否合法
            continue
        b = input("请输入第二个操作数：")
        sum = a+s+b
        if judgezero(sum) == 0:     # 判断是否进行了除0运算
            continue
        print(sum+"={:g}".format(eval(sum)))
        i = int(input("是否继续输入？(输入0结束，输入1对得出的结果继续操作，输入2进行新的运算)"))

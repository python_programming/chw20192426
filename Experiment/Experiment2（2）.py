# -*- encoding: utf-8 -*-
'''
文件:    Experiment2.py
时间:    2020/04/16 19:20:35
作者:    20192426 陈瀚文
'''

# 版本二

import re
from math import*


def splitarg(formula):
    '''
    功能：将数与运算符分离
    '''
    pattern = "[+\-*/%]"
    return re.split(pattern, formula)


def splitmark(formula):
    '''
    功能：将运算符与数分离
    '''
    pattern = "[0-9]"
    mark = re.split(pattern, formula)
    return list(filter(None, mark))


def judge(formula):
    '''
    功能：判断被除数是否为零，若不为零输出运算结果，否则重新输入
    '''
    try:
        print("{}={:g}".format(formula, eval(formula)))
        return str(eval(formula))
    except ZeroDivisionError:
        print("\033[31m0不能作为除数！请重新输入！\033[0m")


def simple(result):
    '''
    功能：四则运算+求余运算+乘方
    '''
    while True:
        formula = input()
        arg = splitarg(formula)         # 把表达式以运算符为界分割成列表
        arg = list(filter(None, arg))   # 将列表中的空字符串删掉
        mark = splitmark(formula)       # 把表达式中的运算符分离出来
        if formula == "q":              # 若输入为q，结束循环
            print("                       Bye~")
            return '0'
        elif formula == "c":            # 若输入为c，切换成计算三角函数模式
            return '2'
        elif len(arg) == 1 and mark != []:  # 若输入的表达式只有一个操作数，则默认上一次计算的结果为另一个操作数
            result = result+formula
            result = judge(result)
        else:                           # 正常输入表达式，正常计算
            result = judge(formula)


def trigonometric(result):
    '''
    功能：计算三角函数
    '''
    while True:
        formula = input("请输入表达式：")
        if formula == "q":              # 若输入为q，结束循环
            print("                       Bye~")
            return '0'
        elif formula == "c":            # 若输入为c，切换成计算三角函数模式
            return '1'
        else:
            arg = re.split("[()]", formula)     # 将三角函数表达式以括号为界分割，组成列表
            arg = list(filter(None, arg))       # 除去列表中的空字符串，若输入正确，此时列表的第一项为三角函数名，第二项为数据
            # 判断表达式所计算的是三角函数还是反三角函数
            if len(arg[0]) == 3:                # 三角函数情况
                arg[1] = eval(arg[1])/180*pi
                formula1 = eval(arg[0]+"("+str(arg[1])+")")
                print("{}={:g}".format(formula, formula1))
            elif len(arg[0]) == 4:              # 反三角函数情况
                formula1 = eval(arg[0]+"("+str(arg[1])+")")*180/pi
                print("{}={:g}".format(formula, formula1))


print('''\033[36m---------------------开始计算---------------------\033[0m\n
\033[33m--------------------输入q退出---------------------\033[0m\n
---------------请直接输入要计算的公式---------------

支持运算类型：
    1.四则运算
    2.求余运算
    3.乘方(用“**”表示，例如2^3表示为2**3)
    4.三角函数计算(直接输入公式，例如：sin(x))
''')

mode = input('''请选择模式，输入对应的数字序号：    
    1.普通计算（对应类型1-3）
    2.三角函数（对应类型4）
    0.退出
    ''')
if mode == '0':
    print("                       Bye~")
while (int(mode)):      # 当mode="0"时，程序退出
    if mode == '1':     # 当mode="1"时，执行四则运算函数
        print("请输入表达式：(输入q退出,输入c切换模式)")
        result = "0"
        mode = simple(result)
    elif mode == '2':   # 当mode="2"时，执行三角函数计算
        print("请输入表达式：(角度使用角度制，反三角函数请使用形如asin（）的形式输出输入q退出，输入c切换模式)")
        result = "0"
        mode = trigonometric(result)
    else:               # 当输入其他值时，报错
        print("\033[31m请正确输入模式序号！\033[0m")

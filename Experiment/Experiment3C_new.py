# -*- encoding: utf-8 -*-
'''
文件:    Experiment3C_new.py
时间:    2020/05/16 13:46:56
作者:    20192426 陈瀚文
'''

import socket
import os
from Cryptodome.Cipher import DES
import base64


def pad(text):
    """
    参考：https://blog.csdn.net/yangxiaodong88/article/details/80801278
    ### 加密函数，如果text不是8的倍数【加密文本text必须为8的倍数！】，那就补足为8的倍数
    :param text:
    :return:
    """
    while len(text) % 8 != 0:
        text += ' '
    return text


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('39.96.51.81', 8080))  # 初始化TCP服务器连接

key = "qwerasdf".encode()   # 将密钥编码为二进制格式
des = DES.new(key, DES.MODE_ECB)    # 创建DES实例

str1 = input("请输入要传输的文件名：")  # 输入文件名
s.sendall("请接收密钥！".encode())  # 提示服务端接收密钥
print("来自 ('39.96.51.81', 8080) 的信息：", s.recv(1024).decode())   # 接收服务端反馈
s.sendall(base64.b64encode(key))    # 发送加密的密钥
print("来自 ('39.96.51.81', 8080) 的信息：", s.recv(1024).decode())   # 接收服务端反馈

s.sendall(str1.encode())  # 发送文件名
os.chdir("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\Experiment")
file = open('{}'.format(str1), 'r', encoding="utf8")  # 打开目标文件
text = file.read()  # 读取文件信息
# 参考了https://blog.csdn.net/yangxiaodong88/article/details/80801278
padded_text = pad(text)
encrypted_text = des.encrypt(padded_text.encode('utf-8'))  # 加密
s.sendall(encrypted_text)  # 发送文件信息
file.close()
data = s.recv(1024)  # 接收反馈信息
print("来自 ('39.96.51.81', 8080) 的信息：", data.decode())  # 打印接收到的信息
s.sendall("收到".encode())  # 回复服务器
s.close()  # 关闭套接字

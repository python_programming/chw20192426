# -*- encoding: utf-8 -*-
'''
文件:    Experiment3S_new.py
时间:    2020/05/16 13:37:02
作者:    20192426 陈瀚文
'''

import socket
import os
from Cryptodome.Cipher import DES
import base64

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('172.17.232.247', 8080))  # 绑定地址到套接字
s.listen()  # 开启TCP监听
conn, address = s.accept()  # 等待连接到来
print("来自", address, "的信息：", conn.recv(1024).decode())    # 接收客户端的信息
conn.sendall("请发送密钥!".encode())    # 向客户端请求密钥
key = base64.b64decode(conn.recv(1024))  # 接收密钥并解密
conn.sendall("已收到密钥!".encode())    # 反馈信息
print("密钥为：", key)  # 输出密钥
des = DES.new(key, DES.MODE_ECB)    # 创建一个DES实例
name = conn.recv(1024)  # 接收文件名
print("来自", address, "的文件：", name.decode())  # 打印接收到的文件名
data = conn.recv(1024)  # 接收文件内容

# 将接收到的内容保存到文件Ex3receive.txt中
f = open("Ex3receive.txt", "w", encoding="utf8")
f.write(des.decrypt(data).decode().rstrip(
    ' '))
f.close()

print("来自", address, "的信息：", des.decrypt(data).decode().rstrip(
    ' '), "已保存为Ex3receive.txt")  # 打印接收到的信息
conn.sendall("服务器已经收到了数据内容！".encode())  # 与客户端进行交互
data = conn.recv(1024)  # 接收反馈
s.close()  # 关闭套接字
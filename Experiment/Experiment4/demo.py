import requests
from bs4 import BeautifulSoup
import re
pattern = r'/weibo?'
url = 'https://s.weibo.com/top/summary/summary?cate=realtimehot'
#
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36 Edg/83.0.478.44"}
wb_data = requests.get(url, headers=headers)
wb_data.encoding = "utf-8"
soup = BeautifulSoup(wb_data.content, 'lxml')
result = []
for k in soup.find_all('a'):
    link = k.get('href_to')
    if link and re.match(pattern, link):
        result.append("https://s.weibo.com" + link)
print(result)
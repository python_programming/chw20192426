# -*- encoding: utf-8 -*-
'''
文件:    数据库实践.py
时间:    2020/04/29 22:17:37
作者:    20192426 陈瀚文
'''

import pymysql
# 创建连接
conn = pymysql.connect(host="127.0.0.1", user='root', charset='utf8')
# 创建游标
cursor = conn.cursor()
# 创建数据库
cursor.execute("create database if not exists final_exam default character set utf8")   # 默认使用utf8字符集
conn.commit()
conn.close()
conn = pymysql.connect(host="127.0.0.1", user='root',database='final_exam', charset='utf8')
cursor = conn.cursor()
# 创建一个表
cursor.execute("create table if not exists student (number int(10) primary key, name varchar(20), score float(2))")
conn.commit()
# 插入三个记录
cursor.execute("insert into student (number,name,score) values (20192426,'张三',90)")
cursor.execute("insert into student (number,name,score) values (20192427,'李四',120)")
cursor.execute("insert into student (number,name,score) values (20192432,'王五',100)")
conn.commit()
# 查询记录
print("插入记录后数据库的数据：共", cursor.execute("select * from student"),"条",cursor.fetchall())
# 修改记录
print("满分为100分，对20192427的错误数据进行修改")
cursor.execute("update student set score = 100 where number=20192427")
conn.commit()
# 查询记录
print("修改后数据库的数据：共", cursor.execute("select * from student"),"条",cursor.fetchall())
# 删除记录
print("1924班只有31个人，删除错误数据。")
cursor.execute("delete from student where number=20192432")
conn.commit()
# 查询记录
print("删除后数据库的数据：共", cursor.execute("select * from student"),"条",cursor.fetchall())
# 关闭游标对象
cursor.close()
# 关闭数据库
conn.close()

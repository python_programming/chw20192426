# -*- encoding: utf-8 -*-
'''
文件:    客户端.py
时间:    2020/05/06 21:31:40
作者:    20192426 陈瀚文
'''

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 8080))  # 初始化TCP服务器连接
while True:     # 建立循环反复向服务端发送信息，直至确认循环结束
    str1 = input("请输入要传输的内容：")
    s.sendall(str1.encode())
    data = s.recv(1024)
    print(data.decode())    # 打印接收到的数据
    judge = input('是否继续发送信息？（yes or no）')   # 判断是否继续进行循环
    if judge in ["yes", "y"]:
        pass
    elif judge in ["no", "n"]:
        break
    else:
        print("\033[31merror!\nplease input yes or no!\033[0m")
s.close()   # 关闭套接字

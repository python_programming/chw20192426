# -*- encoding: utf-8 -*-
'''
文件:    文件C.py
时间:    2020/05/07 17:45:42
作者:    20192426 陈瀚文
'''

import socket
import os

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 8080))  # 初始化TCP服务器连接

str1 = input("请输入要传输的文件名：")
s.sendall(str1.encode())    # 发送文件名
os.chdir("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\Lesson 11\\文件传输")
file = open('{}'.format(str1), 'r', encoding='utf8')
text = file.read()  # 读取文件信息

s.sendall(text.encode())    # 发送文件信息
file.close()
data = s.recv(1024)  # 接收反馈信息
print("来自 ('127.0.0.1', 8080) 的信息：", data.decode())  # 打印接收到的信息
s.sendall("收到".encode())  # 回复服务器
name = s.recv(1024)
print("来自 ('127.0.0.1', 8080) 的文件：", name.decode())
data = s.recv(1024)  # 接收文件数据
f = open("reply.txt", "w", encoding="utf8")
f.write(data.decode())
f.close()
print("来自 ('127.0.0.1', 8080) 的信息：", data.decode())
s.sendall("已成功接收，中断连接！".encode())    # 发送反馈信息
s.close()  # 关闭套接字

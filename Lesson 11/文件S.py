# -*- encoding: utf-8 -*-
'''
文件:    文件S.py
时间:    2020/05/07 17:55:49
作者:    20192426 陈瀚文
'''

import socket
import os

os.chdir("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\Lesson 11\\文件传输")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 8080))  # 绑定地址到套接字
s.listen()  # 开启TCP监听
conn, address = s.accept()  # 等待连接到来
name = conn.recv(1024)
print("来自", address, "的文件：", name.decode())  # 打印接收到的文件
data = conn.recv(1024)  # 接收文件内容
f = open("receive.txt", "w", encoding="utf8")
f.write(data.decode())
f.close()
print("来自", address, "的信息：", data.decode(), "已保存为receive.txt")  # 打印接收到的信息
conn.sendall("服务器已经收到了数据内容,准备传输文件，注意接收！".encode())  # 与客户端进行交互
data = conn.recv(1024)  # 接收反馈
conn.sendall("reply.txt".encode())  # 发送文件名
f = open("receive.txt", "r", encoding="utf8")
data = f.read()
conn.sendall(data.encode())  # 发送文件信息
f.close()
data = conn.recv(1024)
print("来自", address, "的信息：", data.decode())
s.close()  # 关闭套接字

# -*- encoding: utf-8 -*-
'''
文件:    服务端.py
时间:    2020/05/06 21:11:32
作者:    20192426 陈瀚文
'''

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1', 8080))  # 绑定地址到套接字
s.listen()  # 开启TCP监听
conn, address = s.accept()  # 等待连接到来
data = conn.recv(1024)
while True:  # 建立循环重复接收信息，直至客户端断开连接
    if len(data) == 0:  # 当客户端断开连接时，循环结束
        break
    print("来自", address, "的信息：", data.decode())  # 打印接收到的信息
    conn.sendall(("服务器已经收到了数据内容：" + str(data.decode())).encode())  # 与客户端进行交互
    data = conn.recv(1024)
s.close()  # 关闭套接字

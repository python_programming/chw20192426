# -*- encoding: utf-8 -*-
'''
文件:    python语言基础实践.py
时间:    2020/03/04 19:02:43
作者:    20192426 陈瀚文
'''

print("已知：E(x)=(5x+8) mod 26函数，解密函数就是D(x) = 21(x - 8) mod 26 。")
print("要求：输入一个x，计算E(x)的值y。然后计算D(y)并判断x是否等于D（y）。")

number = int(input("please input a number:"))      # 获取数字
print("origin number is {}".format(number))

encrypt = (5*number+8) % 26                          # 加密数字
print("encrypt number is {}".format(encrypt))

decrypt = (21*(encrypt-8) % 26)                      # 解密数字
print("decrypt number is {}".format(decrypt))

if decrypt == number:                                # 判断解密的数字与原数字是否一致
    print("origin number is equal to decrypt number")
else:
    print("origin number is not equal to decrypt number")


print("要求：输入3个数a，n，q（q≠1），求Sn？")

a, n, q = map(int, input("please input First item, number of items,\
common ratio:(use space to split every number)").split(' '))  # 获取首项、项数、公比
if q == 1:                                                    # 若q为1,执行
    Sn = a*n
else:                                                         # 若q不为1，执行
    Sn = a*(q**(n-1))/(q-1)
    print(Sn)

# -*- encoding: utf-8 -*-
'''
文件:    仿射密码改.py
时间:    2020/03/05 09:26:51
作者:    20192426 陈瀚文
'''

print("已知：E(x)=(5x+8) mod 26函数，解密函数就是D(x) = 21(x - 8) mod 26 。")
print("要求：输入一个字母，计算E(x)的值y。然后计算D(y)并判断x是否等于D（y）。")

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
word = input("please input a capital letter:")
number = alphabet.index(word)
print("origin number is {}".format(number))

encrypt=(5*number+8)%26
print("encrypt number is {}".format(encrypt))

decrypt=(21*(encrypt-8)%26)
print("decrypt number is {}".format(decrypt))

if decrypt == number:
    print("origin number is equal to decrypt number")

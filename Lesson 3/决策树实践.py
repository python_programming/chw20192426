# -*- encoding: utf-8 -*-
'''
文件:    决策树实践.py
时间:    2020/03/11 20:20:37
作者:    20192426 陈瀚文
'''

while True:                                                                                             # 构建循环，最后要使用break退出
    result = input("Continue to find a job?(yes or no)")                           
    if result in ["yes", "y"]:                                                                          # 判断是否继续运行程序
        money = int(input("Continue.\nHow much will the company offer?($)"))                            
        if money >= 50000:                                                                              # 判断工资是否达到预期
            time = input("Continue.\nWill it take you more than 1 hour to commute?(yes or no)")         
            if time in ["no", "n"]:                                                                     # 判断通勤时间是否符合要求
                coffee = input("Continue.\nWill this company offer free coffee?(yes or no)")            
                if coffee in ["yes", "y"]:                                                              # 判断是否提供咖啡
                    print(
                        "\033[32mAll needs are met!\nAccept the offer!\033[0m")
                elif coffee in ["no", "n"]:
                    print(
                        "\033[31mThis company doesn't offer free coffee!\ndecline the offer!\033[0m")
                else:
                    print("\033[31merror!\nplease input yes or no!\033[0m")                             # 特殊情况处理
            elif time in ["yes", "y"]:
                print(
                    "\033[31mThis company is far from your home!\ndecline the offer!\033[0m")
            else:
                print("\033[31merror!\nplease input yes or no!\033[0m")                                 # 特殊情况处理
        else:
            print("\033[31mThe money is below your expectation.\nDecline the offer!\033[0m")

    elif result in ["no", "n"]:                                                                         # 程序退出
        break
    else:
        print("\033[31merror!\nplease input yes or no!\033[0m")                                         # 特殊情况处理

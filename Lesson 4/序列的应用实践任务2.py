# 本数据为b站的『电影』投稿在 2020年03月11日 - 2020年03月18日 的数据综合得分
movies = [('紫罗兰永恒花园外传：永远与自动手记人偶', 2272848), ('环太平洋', 2645286), ('城市猎人', 869134),
          ('人工智能', 407202), ('倩女幽魂2：人间道', 563328), ('旺角卡门', 532898), ('功夫', 260988),
          ('倩女幽魂3：道道道', 316501), ('倩女幽魂', 273157), ('神探夏洛克：可恶的新娘', 268373)]
# 删除电影
print("\033[33m{:-^125}\033[0m".format('删除前'))
print(movies)
del movies[-1]
print("\033[33m{:-^125}\033[0m".format('删除后'))
print(movies)
# 添加电影
print("\033[33m{:-^125}\033[0m".format('添加'))
movies.append(('警察故事', 117474))
print(movies)
# 修改电影
print("\033[33m{:-^125}\033[0m".format('修改'))
movies[-1] = ('神探夏洛克：可恶的新娘', 268373)
print(movies)
# 将电影按综合评分由高到低排序
confirm = input("\033[32m是否将电影按评分由高到低排序（y or n）\033[0m")
if confirm in ['yes', 'y']:
    movies = sorted(movies, key=lambda movies: movies[1], reverse=True)
    print(movies)
elif confirm in ['no', 'n']:
    print("程序结束！")
else:
    print("请正确输入！")

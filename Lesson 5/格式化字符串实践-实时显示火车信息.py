# -*- encoding: utf-8 -*-
'''
文件:    格式化字符串实践-实时显示火车信息.py
时间:    2020/03/25 21:15:32
作者:    20192426 陈瀚文
'''

update = input("是否需要更新车票信息？(yes or no)：")
info = []                                                                       # 初始化列表
n = 1
if update in ['yes', 'y']:                                                      # 判断是否更新
    while n:
        train = input("请输入车次:")                                             # 获取车次信息
        start = input("请输入出发站:")                                           # 获取出发站
        stop = input("请输入到达站:")                                            # 获取到达站
        gtime = input("请输入出发时间:")                                         # 获取出发时间
        stime = input("请输入到达时间:")                                         # 获取到达时间
        dtime = input("请输入历时:")                                             # 获取历时
        info.append([train, start, stop, gtime, stime, dtime])                  # 将获取到的六条信息以列表形式保存到info列表中
        print("更新后的火车信息:")                                              
        print("{0:{5}^4}\t{1:{5}^4}\t{2:{5}^4}\t{3:{5}^4}\t{4:{5}^4}".format(
            '车次', '出发站-到达站', '出发时间', '到达时间', '历时', chr(12288)))
        for i in range(len(info)):                                              # 遍历输出info列表中的子列表的每一个元素（接下一行注释）
            print("{0:{5}^6}\t{1:{5}^6}\t{2:{5}^6}\t{3:{5}^6}\t{4:{5}^6}".format(   #（接上一行注释）chr(12288)代表中文空格，使输出结果更美观
                info[i][0], info[i][1]+'-'+info[i][2], info[i][3], info[i][4], info[i][5], chr(12288)))
        update = input("是否继续更新车票信息？(yes or no)：")                     # 判断是否继续更新列表信息
        if update in ["yes", "y"]:
            n = 1
        elif update in ["no", "n"]:
            n = 0
        else:
            print("\033[31merror!\nplease input yes or no!\033[0m")
            break
elif update in ["no", "n"]:
    print("不进行更新操作！")
else:
    print("\033[31merror!\nplease input yes or no!\033[0m")

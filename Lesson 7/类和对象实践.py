# -*- encoding: utf-8 -*-
'''
文件:    类和对象实践.py
时间:    2020/04/08 21:18:10
作者:    20192426 陈瀚文
'''

# 定义学生类
class Student():
    """学生类"""
    # 定义私有变量计算已输入的人数，在_sumnumber()方法中使用
    __number = 1

    def __init__(self):
        print("\n学生", Student.__number)

    # 定义方法将学生信息输出
    def setinfo(self, name, gender, age):
        Student.name = name
        Student.gender = gender
        Student.age = age
        Student.__number += 1
        print("姓名:", Student.name, "性别:", Student.gender, "年龄:", Student.age)

    # 定义受保护的类输出已输入的学生人数
    def _sumnumber(self):
        print("\n已输入{}位学生。".format(Student.__number-1))


# 定义学生类的子类:科目类
class major(Student):
    # 定义类的构造函数，使其输出专业名
    def __init__(self, name):
        major.name = name
        print("该生的专业为:", major.name)

        # 继承父类的方法，输出已输入的学生人数
        Student._sumnumber(self)

# 构建循环反复输入学生信息，直到确认退出时循环结束
while(True):
    name = input("请输入学生姓名：")
    gender = input("请输入学生性别：")
    age = input("请输入学生年龄：")
    majorname = input("请输入学生专业：")
    student = Student()
    student.setinfo(name, gender, age)
    student1 = major(majorname)

    judge = input("是否继续输入？(y or n)")
    if judge in ["yes", "y"]:
        pass
    elif judge in ["no", "n"]:
        break
    else:
        print("\033[31merror!\nplease input yes or no!\033[0m")
        break

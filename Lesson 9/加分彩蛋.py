# 加分1、2
import os
from docx import Document
from Cryptodome.Cipher import DES
import binascii
import xlwt


def pad(text):
    """
    参考：https://blog.csdn.net/yangxiaodong88/article/details/80801278
    # 加密函数，如果text不是8的倍数【加密文本text必须为8的倍数！】，那就补足为8的倍数
    :param text:
    :return:
    """
    while len(text) % 8 != 0:
        text += ' '
    return text


# 创建文件
# 判断文件目录1924是否存在，若不存在，新建，若存在，进入该目录
if os.path.exists("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\Lesson 9\\1924"):
    os.chdir("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\Lesson 9\\1924")
else:
    os.mkdir("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\Lesson 9\\1924")
Doc = Document()    # 初始化doc文档
Doc.add_paragraph("Python is good!")    # 添加两行信息
Doc.add_paragraph("Hello,Python!")
Doc.save("明文.doc")

# 加密,参考了https://blog.csdn.net/yangxiaodong88/article/details/80801278
key = b"qwerasdf"
des = DES.new(key, DES.MODE_ECB)  # 创建一个DES实例
Doc = Document("明文.doc")
text = ""
for para in Doc.paragraphs:       # 读取文档中的内容
    text = text+"\n"+para.text
Doc.save("明文.doc")              # 关闭文件

padded_text = pad(text)         # 参考了https://blog.csdn.net/yangxiaodong88/article/details/80801278
encrypted_text = des.encrypt(padded_text.encode('utf-8'))  # 加密
Doc = Document()
Doc.add_paragraph(str(encrypted_text))  # 将密文添加到doc文档中
Doc.save("密文.doc")
# 解密,参考了https://blog.csdn.net/yangxiaodong88/article/details/80801278
Doc = Document()
plain_text = des.decrypt(encrypted_text).decode().rstrip(
    ' ')  # 解密,rstrip(' ')返回从字符串末尾删除所有字符串的字符串(默认空白字符)的副本
Doc.add_paragraph(plain_text)
Doc.save("解密文件.doc")
# 加分3
workbook = xlwt.Workbook(encoding = 'utf-8')
worksheet = workbook.add_sheet('My Worksheet')
style = xlwt.XFStyle()  # 初始化样式
font = xlwt.Font()      # 为样式创建字体
font.name = 'Consolas' 
style.font = font       # 设定样式
worksheet.write(0, 0, "学号") # 写入数据
worksheet.write(0, 1, "姓名")
worksheet.write(0, 2, "科目")
worksheet.write(0, 3, "成绩")
worksheet.write(1, 0, "20192426") 
worksheet.write(1, 1, "陈瀚文")
worksheet.write(1, 2, "python程序设计")
worksheet.write(1, 3, "100")


workbook.save('1924.xls') # 保存文件

import os
# 判断文件目录1924是否存在，若不存在，新建，若存在，进入该目录
if os.path.exists("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\Lesson 9\\1924"):
    os.chdir("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\Lesson 9\\1924")
else:
    os.mkdir("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\Lesson 9\\1924")
file = open("192426.txt", "w")          # 新建文件192426.txt
# 给文件写入信息
file.write("{0:^8}\t{1:^4}\t{2:^10}\t{3:^4}\n".format("学号", "姓名", "科目", "成绩"))
file.write("{0:^10}\t{1:^4}\t{2:^10}\t{3:^4}\n".format(
    "20192426", "陈瀚文", "python程序设计", "100"))
# 关闭文件
file.close()
# 在刚才的文件中追加文件内容
file = open("192426.txt", "a")
file.write("{0:^10}\t{1:^4}\t{2:^10}\t{3:^4}\n".format(
    "20192405", "张纹豪", "python程序设计", "100"))
file.write("{0:^10}\t{1:^4}\t{2:^10}\t{3:^4}\n".format(
    "20192411", "何张榕", "python程序设计", "100"))
# 关闭文件
file.close()
# 读取文件的信息
file = open("192426.txt", "r")
print(file.read())
file.close()
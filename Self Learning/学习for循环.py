# -*- encoding: utf-8 -*-
'''
文件:    学习for循环.py
时间:    2020/03/09 22:14:13
作者:    20192426 陈瀚文
'''

for i in range(1,11,2):    # 第三个参数指示步长
    print(i)

# 纵向输出字符
string = '不要再说我不能'
print(string)
for ch in string:
    print(ch)
# -*- encoding: utf-8 -*-
'''
文件:    打印九九乘法表.py
时间:    2020/03/09 22:21:18
作者:    20192426 陈瀚文
'''

for i in range(1,10):
    for j in range(1,i+1):
        print(str(j)+"×"+str(i)+"="+str(j*i)+"\t",end='')
    print("")
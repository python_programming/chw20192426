import os
import openpyxl


def Remove(list):
    '''
    去除列表中的最大值和最小值并对剩下的值求平均数
    '''
    list.remove(max(list))
    list.remove(min(list))
    average = sum(list)/len(list)
    return average


os.chdir("D:\\网空专业\\大一下\\Python程序设计\\Learnpython\\文件操作\\score")
file = open("score.txt", "r")

data = []
line = "line"
score = []
score1 = []
score2 = []
score3 = []

while line:
    line = file.readline()
    listline = line.rstrip("\n")
    listline = listline.split(",")
    data.append(listline)

file.close()

for i in range(10):
    score1.append(eval(data[i][1]))
    score2.append(eval(data[i][2]))
    score3.append(eval(data[i][3]))

score.append(Remove(score1))
score.append(Remove(score2))
score.append(Remove(score3))

outcome = [1+score.index(max(score)), max(score)]
workbook = openpyxl.Workbook()
worksheet = workbook.create_sheet('score')
worksheet.cell(1, 1).value = outcome[0]
worksheet.cell(1, 2).value = outcome[1]
workbook.save('score.xlsx')

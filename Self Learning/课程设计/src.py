# -*- encoding: utf-8 -*-
'''
文件:    code.py
时间:    2020/06/26 23:06:52
作者:    20192426 陈瀚文
'''

import pandas as pd
import matplotlib.pyplot as plt
import itertools

# 防止中文出现乱码
plt.rcParams['font.sans-serif'] = ['SimHei']  # 黑体
plt.rcParams['axes.unicode_minus'] = False

Origin_Data = pd.read_csv("12.nino3.long.anom.data.csv")    # 读取文件
Origin_Data.dropna()    # 丢弃缺失值

file1 = open("nino3_dropnan.txt", "w")  # 新建文件用于保存初步处理后的数据
file1.write("Date,Nino3\n") # 在文件的第一行写上表头

# 通过循环遍历数据表并将数据写入文件中
for i in range(Origin_Data.shape[0]):
    for j in range(1, Origin_Data.shape[1]):
        file1.write("{}-{:0>2}-01,{}\n".format(str(Origin_Data.iloc[i, 0]), j, str(Origin_Data.iloc[i, j])))
file1.close()   # 至此，第一步已完成

data1 = pd.read_csv("nino3_dropnan.txt")  # 读取刚刚生成的文件

# 获取最大值、最小值、平均值
MaxValue = data1.describe().at['max', 'Nino3']
MinValue = data1.describe().at['min', 'Nino3']
MeanValue = data1.describe().at['mean', 'Nino3']  # 至此，第二步完成

category = [MinValue, -0.5, 0, 0.5, MaxValue]
label = ['LaNinaTemp', 'Cold', 'Warm', 'NinoTemp']
result = pd.cut(data1['Nino3'], category, labels=label) # 将Nino3这一列的结果用label进行离散化处理
data1["Label"] = result # 新建一列label将result存入其中
data1.to_csv("nino3_dropnan_result.csv", index=False)   # 将修改后的data1保存

# 生成饼状图
result_counts = result.value_counts()   # 统计result中各字符串出现的次数
plt.figure()
result_counts.plot(kind='pie', figsize=(12, 8), autopct='%.2f%%')
plt.title("数据离散化统计饼状图")   # 设置图片的标题
plt.savefig("nino3_ pie.png", dpi=400)
plt.show()  # 至此，第三步已完成

num_times = [(k, len(list(v))) for k, v in itertools.groupby(result)]
LaNinaList = []  # 存储LaNina事件开始的时间
NinoList = []  # 存储Nino事件开始的时间
for i in range(len(num_times)):
    if num_times[i][0] == 'LaNinaTemp' and num_times[i][1] >= 6:
        sumIndex = 0
        j = i
        while j >= 1:
            sumIndex += num_times[j - 1][1]
            j -= 1
        LaNinaList.append(data1.iloc[sumIndex, 0])
    if num_times[i][0] == 'NinoTemp' and num_times[i][1] >= 6:
        sumIndex = 0
        j = i
        while j >= 1:
            sumIndex += num_times[j - 1][1]
            j -= 1
        NinoList.append(data1.iloc[sumIndex, 0])

LaNina = open("LaNinaStartDate.txt", 'w')
for item in LaNinaList:
    LaNina.write(item + '\n')
LaNina.close()
Nino = open("NinoStartDate.txt", 'w')
for item in NinoList:
    Nino.write(item + '\n')
Nino.close()  # 至此，第四步已完成

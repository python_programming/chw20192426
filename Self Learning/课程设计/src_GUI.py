# -*- encoding: utf-8 -*-
'''
文件:    keshe1.py
时间:    2020/06/29 12:53:47
作者:    20192426 陈瀚文
'''

import pandas as pd
import matplotlib.pyplot as plt
import itertools
import tkinter as tk
from tkinter import filedialog

# 防止中文出现乱码
plt.rcParams['font.sans-serif'] = ['SimHei']  # 黑体
plt.rcParams['axes.unicode_minus'] = False
var = None


def dataPreprocessing(var):
    """
    功能：完成第一步的要求
    """
    while True:
        var.set('请选择要打开的文件12.nino3.long.anom.data.csv:')
        try:
            fileName = filedialog.askopenfilename(title='Select the file', filetypes=[('All Files', '*')])
            Origin_Data = pd.read_csv(fileName)  # 读取文件
            Origin_Data.dropna()  # 丢弃缺失值

            file1 = open("nino3_dropnan.txt", "w")  # 新建文件用于保存初步处理后的数据
            file1.write("Date,Nino3\n")  # 在文件的第一行写上表头

            # 通过循环遍历数据表并将数据写入文件中
            for i in range(Origin_Data.shape[0]):
                for j in range(1, Origin_Data.shape[1]):
                    file1.write("{}-{:0>2}-01,{}\n".format(str(Origin_Data.iloc[i, 0]), j, str(Origin_Data.iloc[i, j])))
            file1.close()  # 至此，任务一已完成
            var.set('任务一执行成功')
            break
        except:
            var.set('文件错误，任务执行失败，请核对后重新执行！')
            return False


def dataGroup(var):
    """
    功能：完成第二步的要求
    """
    while True:
        global MaxValue
        global MinValue
        global MeanValue
        var.set('请选择要打开的文件nino3_dropnan.txt:')
        fileName2 = filedialog.askopenfilename(title='Select the file', filetypes=[('All Files', '*')])
        try:
            data1 = pd.read_csv(fileName2)  # 读取刚刚生成的文件

            # 获取最大值、最小值、平均值
            MaxValue = data1.describe().at['max', 'Nino3']
            MinValue = data1.describe().at['min', 'Nino3']
            MeanValue = data1.describe().at['mean', 'Nino3']  # 至此，任务二完成
            var.set('任务二执行成功')
            break
        except:
            var.set('文件错误，任务执行失败，请核对后重新执行！')
            return False


def dataDescribeVisualiztion(var):
    """
    功能：完成第三步的要求
    """
    while True:
        var.set('请选择要打开的文件nino3_dropnan.txt:')
        fileName3 = filedialog.askopenfilename(title='Select the file', filetypes=[('All Files', '*')])
        try:
            global result
            data1 = pd.read_csv(fileName3)
            category = [MinValue, -0.5, 0, 0.5, MaxValue]
            label = ['LaNinaTemp', 'Cold', 'Warm', 'NinoTemp']
            result = pd.cut(data1['Nino3'], category, labels=label)  # 将Nino3这一列的结果用label进行离散化处理
            data1["Label"] = result  # 新建一列label将result存入其中
            data1.to_csv("nino3_dropnan_result.csv", index=False)  # 将修改后的data1保存

            # 生成饼状图
            result_counts = result.value_counts()  # 统计result中各字符串出现的次数
            plt.figure()
            result_counts.plot(kind='pie', figsize=(12, 8), autopct='%.2f%%')
            plt.title("数据离散化统计饼状图")  # 设置图片的标题
            plt.savefig("nino3_ pie.png", dpi=400)
            plt.show()  # 至此，任务三已完成
            var.set('任务三执行成功')
            break
        except:
            var.set('文件错误，任务执行失败，请核对后重新执行！')
            return False


def dataCalculate(var):
    """
    功能：完成第四步的要求
    """
    while True:
        var.set('请选择要打开的文件nino3_dropnan_result.csv:')
        fileName4 = filedialog.askopenfilename(title='Select the file', filetypes=[('All Files', '*')])
        try:
            data1 = pd.read_csv(fileName4)
            num_times = [(k, len(list(v))) for k, v in itertools.groupby(result)]
            LaNinaList = []  # 存储LaNina事件开始的时间
            NinoList = []  # 存储Nino事件开始的时间
            for i in range(len(num_times)):
                if num_times[i][0] == 'LaNinaTemp' and num_times[i][1] >= 6:
                    sumIndex = 0
                    j = i
                    while j >= 1:
                        sumIndex += num_times[j - 1][1]
                        j -= 1
                    LaNinaList.append(data1.iloc[sumIndex, 0])
                if num_times[i][0] == 'NinoTemp' and num_times[i][1] >= 6:
                    sumIndex = 0
                    j = i
                    while j >= 1:
                        sumIndex += num_times[j - 1][1]
                        j -= 1
                    NinoList.append(data1.iloc[sumIndex, 0])

            LaNina = open("LaNinaStartDate.txt", 'w')
            for item in LaNinaList:
                LaNina.write(item + '\n')
            LaNina.close()
            Nino = open("NinoStartDate.txt", 'w')
            for item in NinoList:
                Nino.write(item + '\n')
            Nino.close()  # 至此，任务四已完成
            var.set('任务四执行成功')
            break
        except:
            var.set('文件错误，任务执行失败，请核对后重新执行！')
            return False


def show_window():
    window = tk.Tk()
    window.title('学生成绩数据分析及可视化系统')
    # window.geometry('500x500')  # 设置窗口尺寸
    width = 500  # 设定窗口宽度
    height = 450  # 设定窗口高度
    screenWidth = window.winfo_screenwidth()  # 获取显示区域的宽度
    screenHeight = window.winfo_screenheight()  # 获取显示区域的高度
    left = (screenWidth - width) / 2
    top = (screenHeight - height) / 2

    # 在设定宽度和高度的基础上指定窗口相对于屏幕左上角的偏移位置
    window.geometry("{}x{}+{}+{}".format(width, height, int(left), int(top)))  # 居中：宽度x高度+x偏移+y偏移
    window.resizable(0, 0)  # 使窗口大小不可变
    var = tk.StringVar()
    label_text = tk.Label(window, textvariable=var, bg='grey', font=('微软雅黑 Light', 12), width=45, height=2)
    label_text.place(x=50, y=20)
    task1 = tk.Button(window, text='1.数据读取及预处理', font=('微软雅黑 Light', 12), width=20, height=1,
                      command=lambda: dataPreprocessing(var))
    task1.place(x=155, y=130)
    task2 = tk.Button(window, text='2.数据最值及平均值', font=('微软雅黑 Light', 12), width=20, height=1,
                      command=lambda: dataGroup(var))
    task2.place(x=155, y=200)
    task3 = tk.Button(window, text='3.数据可视化', font=('微软雅黑 Light', 12), width=20, height=1, command=lambda: dataDescribeVisualiztion(var))
    task3.place(x=155, y=270)
    task4 = tk.Button(window, text='4.数据统计', font=('微软雅黑 Light', 12), width=20, height=1, command=lambda: dataCalculate(var))
    task4.place(x=155, y=340)
    label_tip = tk.Label(window, text='请依次点击按钮并选择对应的文件!', font=('微软雅黑 Light', 9), fg='grey')
    label_tip.place(x=270, y=410)
    window.mainloop()


if __name__ == '__main__':
    show_window()

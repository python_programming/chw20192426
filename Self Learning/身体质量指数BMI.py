# -*- encoding: utf-8 -*-
'''
文件:    身体质量指数BMI.py
时间:    2020/03/11 20:20:28
作者:    20192426 陈瀚文
'''

height, weight = eval(input("请输入身高(米)和体重\(公斤)[逗号隔开]: "))
bmi = weight / pow(height, 2)
print("BMI 数值为：{:.2f}".format(bmi))
nat = ""
if bmi <= 18.4:
    nat = "偏瘦"
elif 18.5 <= bmi <=23.9:
    nat = "正常"
elif 24.0<= bmi < 27.9:
    nat = "偏胖"
else:
    nat = "肥胖"
print("中国标准的BMI指标为:'{0}'".format(nat))
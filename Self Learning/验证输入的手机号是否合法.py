import re
pattern = r'(13[4-9]\d{8})|(15[01289]\d{8})$'   # 模式字符串
mobile = input("请输入一个手机号码：")          # 要匹配的手机号码
match = re.match(pattern, mobile)
if match == None:
    print("不是有效的中国移动的手机号码")
else:
    print("是有效的中国移动的手机号码")
